package ru.bokhan.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.bokhan.tm.api.endpoint.ITaskRestEndpoint;
import ru.bokhan.tm.dto.TaskDto;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;
import ru.bokhan.tm.util.SecurityUtil;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @NotNull
    @Override
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public TaskDto save(@NotNull @RequestBody final TaskDto taskDto) {
        taskDto.setUserId(SecurityUtil.getUserId());
        return taskDtoRepository.save(taskDto);
    }

    @Nullable
    @Override
    @GetMapping("/{id}")
    public TaskDto findById(@NotNull @PathVariable("id") final String id) {
        return taskDtoRepository.findByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/exists/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskDtoRepository.existsByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @Override
    @DeleteMapping("/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        taskDtoRepository.deleteByUserIdAndId(SecurityUtil.getUserId(), id);
    }

}
