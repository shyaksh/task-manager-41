package ru.bokhan.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.bokhan.tm.api.endpoint.ITasksRestEndpoint;
import ru.bokhan.tm.dto.TaskDto;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;
import ru.bokhan.tm.util.SecurityUtil;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TasksRestEndpoint implements ITasksRestEndpoint {

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @NotNull
    @Override
    @GetMapping
    public List<TaskDto> findAll() {
        return taskDtoRepository.findAllByUserId(SecurityUtil.getUserId());
    }

    @NotNull
    @Override
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public List<TaskDto> saveAll(@NotNull @RequestBody final List<TaskDto> list) {
        for (TaskDto taskDto : list) {
            taskDto.setUserId(SecurityUtil.getUserId());
            taskDtoRepository.save(taskDto);
        }
        return list;
    }

    @NotNull
    @Override
    @GetMapping("/count")
    public Long count() {
        return taskDtoRepository.countByUserId(SecurityUtil.getUserId());
    }

    @Override
    @PostMapping("/delete")
    public void deleteAll(@NotNull @RequestBody final List<TaskDto> list) {
        list.forEach(taskDto ->
                taskDtoRepository.deleteByUserIdAndId(SecurityUtil.getUserId(), taskDto.getId()));
    }

    @Override
    @DeleteMapping("/all")
    public void deleteAll() {
        taskDtoRepository.deleteAllByUserId(SecurityUtil.getUserId());
    }

}
