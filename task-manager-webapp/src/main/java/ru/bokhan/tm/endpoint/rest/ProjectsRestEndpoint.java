package ru.bokhan.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.bokhan.tm.api.endpoint.IProjectsRestEndpoint;
import ru.bokhan.tm.dto.ProjectDto;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.entity.ProjectRepository;
import ru.bokhan.tm.util.SecurityUtil;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectsRestEndpoint implements IProjectsRestEndpoint {

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Override
    @GetMapping
    public List<ProjectDto> findAll() {
        return projectDtoRepository.findAllByUserId(SecurityUtil.getUserId());
    }

    @NotNull
    @Override
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public List<ProjectDto> saveAll(@NotNull @RequestBody final List<ProjectDto> list) {
        for (ProjectDto projectDto : list) {
            projectDto.setUserId(SecurityUtil.getUserId());
            projectDtoRepository.save(projectDto);
        }
        return list;
    }

    @NotNull
    @Override
    @GetMapping("/count")
    public Long count() {
        return projectDtoRepository.countByUserId(SecurityUtil.getUserId());
    }

    @Override
    @PostMapping("/delete")
    public void deleteAll(@NotNull @RequestBody final List<ProjectDto> list) {
        list.forEach(projectDto ->
                projectRepository.deleteByUserIdAndId(SecurityUtil.getUserId(), projectDto.getId()));
    }

    @Override
    @DeleteMapping("/all")
    public void deleteAll() {
        projectRepository.deleteAllByUserId(SecurityUtil.getUserId());
    }

}
