package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.bokhan.tm.dto.CustomUser;
import ru.bokhan.tm.dto.RoleDto;
import ru.bokhan.tm.entity.Role;
import ru.bokhan.tm.entity.RoleType;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.repository.dto.RoleDtoRepository;
import ru.bokhan.tm.repository.entity.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleDtoRepository roleDtoRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @Nullable final User user = userRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        @NotNull final List<String> roles = new ArrayList<>();
        for (@NotNull final Role role : user.getRoles()) roles.add(role.toString());
        return new CustomUser(
                org.springframework.security.core.userdetails.User
                        .withUsername(user.getLogin())
                        .password(user.getPasswordHash())
                        .roles(roles.toArray(new String[]{}))
                        .build()
        ).withUserId(user.getId());
    }

    @PostConstruct
    public void initUsers() {
        if (userRepository.count() > 0) return;
        create("test", "test");
        createWithRole("admin", "admin", RoleType.ADMINISTRATOR);
    }

    public void create(@Nullable final String login, @Nullable final String password) {
        if (login == null) return;
        if (password == null) return;


        @NotNull final User user = new User();
        @NotNull final RoleDto role = new RoleDto(user.getId());
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        userRepository.save(user);
        roleDtoRepository.save(role);
    }

    public void createWithRole(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null) return;
        if (password == null) return;
        if (roleType == null) return;

        @NotNull final User user = new User();
        @NotNull final RoleDto role = new RoleDto(user.getId());
        role.setRoleType(roleType);
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        userRepository.save(user);
        roleDtoRepository.save(role);
    }

}
