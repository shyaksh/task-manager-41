package ru.bokhan.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.bokhan.tm.entity.User;

@Repository
public interface UserRepository extends AbstractRepository<User> {

    @Nullable User findByLogin(@NotNull final String login);

}
