package ru.bokhan.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.entity.Project;

import java.util.List;

public interface ProjectRepository extends AbstractRepository<Project> {

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteAllByUserId(@NotNull final String userId);

    @NotNull List<Project> findAllByUserId(@NotNull final String userId);

}