package ru.bokhan.tm.repository.dto;

import ru.bokhan.tm.dto.RoleDto;
import ru.bokhan.tm.entity.Role;
import ru.bokhan.tm.repository.entity.AbstractRepository;

public interface RoleDtoRepository extends AbstractDtoRepository<RoleDto> {
}
