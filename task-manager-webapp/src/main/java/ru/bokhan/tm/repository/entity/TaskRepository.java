package ru.bokhan.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.entity.Task;

import java.util.List;

public interface TaskRepository extends AbstractRepository<Task> {

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId);

    @NotNull List<Task> findAllByUserId(@NotNull final String userId);

}