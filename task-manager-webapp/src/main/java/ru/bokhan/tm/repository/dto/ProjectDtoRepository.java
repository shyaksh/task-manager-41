package ru.bokhan.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.ProjectDto;

import java.util.List;

public interface ProjectDtoRepository extends AbstractDtoRepository<ProjectDto> {

    @NotNull List<ProjectDto> findAllByUserId(@NotNull final String userId);

    @Nullable ProjectDto findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    long countByUserId(@NotNull final String userId);

}