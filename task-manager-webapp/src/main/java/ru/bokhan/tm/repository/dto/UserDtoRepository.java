package ru.bokhan.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.bokhan.tm.dto.UserDto;

@Repository
public interface UserDtoRepository extends AbstractDtoRepository<UserDto> {

    @Nullable UserDto findByLogin(@NotNull final String login);

}
