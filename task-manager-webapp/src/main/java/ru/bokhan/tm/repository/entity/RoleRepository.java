package ru.bokhan.tm.repository.entity;

import ru.bokhan.tm.entity.Project;
import ru.bokhan.tm.entity.Role;

public interface RoleRepository extends AbstractRepository<Role>  {
}
