package ru.bokhan.tm.util;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.bokhan.tm.dto.CustomUser;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import java.net.HttpCookie;
import java.util.List;

public interface SecurityUtil {

    @NotNull
    static String getUserId() {
        @NotNull final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        @NotNull final Object principal = authentication.getPrincipal();
        if (principal == null) throw new  AccessDeniedException();
        if (!(principal instanceof CustomUser)) throw new  AccessDeniedException();
        @NotNull final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }

}
