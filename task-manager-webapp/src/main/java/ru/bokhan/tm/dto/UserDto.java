package ru.bokhan.tm.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "tm_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class UserDto extends AbstractDto {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

}
