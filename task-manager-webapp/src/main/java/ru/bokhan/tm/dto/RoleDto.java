package ru.bokhan.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.entity.AbstractEntity;
import ru.bokhan.tm.entity.RoleType;
import ru.bokhan.tm.entity.User;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_role")
public class RoleDto extends AbstractDto {

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @Column
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

    @NotNull
    public RoleDto(@NotNull String userId) {
        this.userId = userId;
    }

}
