package ru.bokhan.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.bokhan.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "tm_project")
public final class ProjectDto extends AbstractDto {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    @DateTimeFormat(pattern = "yyy-MM-dd")
    private Date dateStart;

    @Nullable
    @DateTimeFormat(pattern = "yyy-MM-dd")
    private Date dateFinish;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "user_id")
    private String userId;

    public ProjectDto(
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) {
        super(id);
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
