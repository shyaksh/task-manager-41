package ru.bokhan.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.bokhan.tm.dto.CustomUser;
import ru.bokhan.tm.entity.Project;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.entity.ProjectRepository;

import java.util.List;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @GetMapping("/projects")
    public ModelAndView index(
            @NotNull @AuthenticationPrincipal CustomUser user
    ) {
        @NotNull final List<Project> projects = projectRepository.findAllByUserId(user.getUserId());
        return new ModelAndView("project-list", "projects", projects);
    }

}