package ru.bokhan.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.bokhan.tm.dto.CustomUser;
import ru.bokhan.tm.dto.ProjectDto;
import ru.bokhan.tm.enumerated.Status;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.entity.ProjectRepository;


@Controller
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @ModelAttribute("statuses")
    @NotNull Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/project/create")
    public String create(
            @NotNull @AuthenticationPrincipal final CustomUser user
    ) {
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(user.getUserId());
        projectDtoRepository.save(project);
        return String.format("redirect:/project/edit/%s", project.getId());
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @NotNull @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") final String id
    ) {
        projectRepository.deleteByUserIdAndId(user.getUserId(), id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @NotNull @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") final String id
    ) {
        @Nullable final ProjectDto project = projectDtoRepository.findByUserIdAndId(user.getUserId(), id);
        return new ModelAndView("project-edit", "project", project);
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @NotNull @AuthenticationPrincipal final CustomUser user,
            @NotNull @ModelAttribute("project") final ProjectDto project,
            BindingResult result
    ) {
        project.setUserId(user.getUserId());
        projectDtoRepository.save(project);
        return "redirect:/projects";
    }

}
