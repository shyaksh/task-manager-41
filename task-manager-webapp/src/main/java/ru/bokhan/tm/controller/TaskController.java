package ru.bokhan.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.bokhan.tm.dto.CustomUser;
import ru.bokhan.tm.dto.ProjectDto;
import ru.bokhan.tm.dto.TaskDto;
import ru.bokhan.tm.enumerated.Status;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;
import ru.bokhan.tm.repository.entity.TaskRepository;

import java.util.List;


@Controller
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @NotNull
    @ModelAttribute("projects")
    private List<ProjectDto> getProjects() {
        return projectDtoRepository.findAll();
    }

    @NotNull
    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/task/create")
    public String create(
            @NotNull @AuthenticationPrincipal final CustomUser user
    ) {
        @NotNull final TaskDto task = new TaskDto();
        task.setUserId(user.getUserId());
        taskDtoRepository.save(task);
        return String.format("redirect:/task/edit/%s", task.getId());
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @NotNull @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") final String id
    ) {
        taskRepository.deleteByUserIdAndId(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @NotNull @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") final String id
    ) {
        @Nullable final TaskDto task = taskDtoRepository.findByUserIdAndId(user.getUserId(), id);
        return new ModelAndView("task-edit", "task", task);
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @NotNull @AuthenticationPrincipal final CustomUser user,
            @NotNull @ModelAttribute("task") final TaskDto task,
            BindingResult result
    ) {
        // TODO FIX https://github.com/spring-projects/spring-framework/issues/11714
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        task.setUserId(user.getUserId());
        taskDtoRepository.save(task);
        return "redirect:/tasks";
    }

}
