package ru.bokhan.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "tm_project")
public final class Project extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Task> tasks;

    public Project(
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) {
        super(id);
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
