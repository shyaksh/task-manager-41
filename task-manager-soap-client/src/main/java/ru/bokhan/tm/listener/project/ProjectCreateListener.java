package ru.bokhan.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.service.ISessionService;
import ru.bokhan.tm.endpoint.soap.ProjectDto;
import ru.bokhan.tm.endpoint.soap.ProjectEndpoint;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;
import ru.bokhan.tm.util.TerminalUtil;

@Component
public final class ProjectCreateListener extends AbstractListener {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    @EventListener(condition = "@projectCreateListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        sessionService.insertTo(projectEndpoint);
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setName(name);
        projectDto.setDescription(description);
        projectEndpoint.saveProject(projectDto);
        System.out.println("[OK]");
    }

}
