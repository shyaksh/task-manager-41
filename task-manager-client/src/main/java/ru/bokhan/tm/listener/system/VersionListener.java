package ru.bokhan.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;

@Component
public final class VersionListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "version";
    }

    @NotNull
    @Override
    public String argument() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    @EventListener(condition = "@versionListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}
