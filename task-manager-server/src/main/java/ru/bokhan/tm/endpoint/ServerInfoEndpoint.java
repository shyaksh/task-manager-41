package ru.bokhan.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bokhan.tm.api.endpoint.IServerInfoEndpoint;
import ru.bokhan.tm.api.service.IPropertyService;

import javax.jws.WebMethod;
import javax.jws.WebService;

@Controller
@WebService
@AllArgsConstructor
@NoArgsConstructor
public final class ServerInfoEndpoint extends AbstractEndpoint implements IServerInfoEndpoint {

    @NotNull
    @Autowired
    IPropertyService propertyService;

    @Override
    @NotNull
    @WebMethod
    public String getServerHost() {
        return propertyService.getServerHost();
    }

    @Override
    @NotNull
    @WebMethod
    public Integer getServerPort() {
        return propertyService.getServerPort();
    }

}
