package ru.bokhan.tm.exception.empty;

public final class EmptyRoleException extends RuntimeException {

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }

}